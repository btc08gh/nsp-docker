#!/bin/bash
git config --global --add safe.directory '*'
git config --global init.defaultBranch main

apk del $(apk info|grep php8)
apk del php8-json php8-zlib php8-common

rm -rf /etc/php8 /usr/lib/php8

apk add gmp-dev zstd zstd-dev gcc php7 php7-dev musl-dev make php7-fpm php7-curl php7-openssl php7-gmp php7-json

perl -pi -e "s/php8/php7/g" /etc/cont-init.d/14-php 
perl -pi -e "s/fpm8/fpm7/g" /etc/services.d/php-fpm/run 

cp /custom-files/php-www2.conf /config/php/www2.conf
cp /custom-files/nginx-default.conf /config/nginx/site-confs/default.conf

if ! grep -q yazstd.so /config/php/php-local.ini;
then
mkdir /tmp/build; cd /tmp/build
git clone --recursive --depth=1 https://github.com/proconsule/php-ext-yazstd && cd php-ext-yazstd && phpize && ./configure && make && make install && cat yazstd.ini >> /config/php/php-local.ini
fi

cd /config/www
rm index.html
git init
git pull https://gitlab.com/izenn/nsp-indexer.git

chown -R abc:abc /config/www
cp /custom-files/prod.keys /config
chown abc:abc /config/prod.keys

if [[ -f /config/www/config.php ]]; then
	GAMESDIR=$(grep gameDir /config/www/config.php|cut -d\" -f2)
fi

if [[ -z $GAMESDIR ]]; then
	GAMESDIR=$(grep gameDir /config/www/config.default.php|cut -d\" -f2)
fi

chown -R abc:abc $GAMESDIR
