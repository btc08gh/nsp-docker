## Usage
To start, 2 manual actions are needed to be made because they are environment specific:
1. copy prod.keys into the custom-files directory
2. edit `docker-compose.yml` and change the source directory for /data/games

Once that is done you are ready to build.
```
docker-compose up --build
```
Once the build is complete press ctrl-c once to gracefully stop the container

Now you can use the following to start the container.  Start is much quicker than build.
```
docker start nsp-indexer
```
When the container is up, you'll be able to access it through http://your.ip:8000

### Tips
* if you need a prod.keys file, you can generate one with lockpick_RCM. (gbatemp.net is your friend)

### Docker Environmental Variables
These variables are documented in config.default.php which can be found here: https://gitlab.com/izenn/nsp-indexer/
```
PHP_MEMORY_LIMIT
NSPINDEXER_GAMES_DIR
NSPINDEXER_CONTENT_URL
NSPINDEXER_EXTENSIONS
NSPINDEXER_ENABLE_NETINSTALL
NSPINDEXER_ENABLE_RENAME
NSPINDEXER_SWITCH_IP
NSPINDEXER_NETINSTALL_SRC
NSPINDEXER_KEYFILE
```
